-module(ms).
-export([start/1, to_slave/2]).

start(N) -> 
	register(master_pid, spawn(fun() -> master(N) end)), 
	true.

to_slave(Message, N) ->
	master_pid ! {Message, N}.

create_slave(Index) ->
	Slave_PID = spawn_link(fun() -> slave() end),
	put(Slave_PID, Index), 
	put(Index, Slave_PID).

remove_slave(Pid) ->
	Index = get(Pid),
	erase(Pid),
	erase(Index),
	Index.

master(0) ->
	process_flag(trap_exit, true),
	receive
		{Message, N} -> get(N) ! Message, master(0);
		{'EXIT', Pid, Reason} -> 
			io:format("Process ~p terminated with reason ~p, restarting.~n", [Pid, Reason]),
			create_slave(remove_slave(Pid)),
			master(0)
	end;
master(N) ->
	create_slave(N),
	master(N-1).

slave() ->	
	receive
		die -> exit("Die message");
		Message -> io:format("Process ~p received message ~p.~n", [self(), Message]), slave()
	end.
