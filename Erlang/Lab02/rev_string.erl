-module(rev_string).
-export([long_reversed_string/2, substring/2]).

long_reversed_string(S, N) ->
	register(master_pid, self()),
	process_flag(trap_exit, true),
	spawn_slaves(S, N, length(S) div N),
	wait_reply([], N).

substring(_, 0) -> [];
substring([H|TL], N) -> [H|substring(TL, N - 1)].

spawn_slaves(_S, 0, _) -> 
	ok;
spawn_slaves(S, 1, _Size) -> 
	% avoids rounding
	spawn_link(fun () -> reverse_string(S, 1) end),
	spawn_slaves(S, 0, _Size);
spawn_slaves(S, N, Size) -> 
	spawn_link(fun () -> reverse_string(substring(S, Size), N) end),
	spawn_slaves(lists:nthtail(Size, S), N - 1, Size).

wait_reply(L, 0) -> 
	unregister(master_pid),
	{reply, L};
wait_reply(L, N) ->
	receive
		{'EXIT', _Pid, normal} -> wait_reply(L, N);
		{'EXIT', Pid, Reason} -> unregister(master_pid), {error, Pid, Reason};
		{string, N, S} -> wait_reply(S ++ L, N - 1)
	end.

reverse_string(S, Client_num) ->
	master_pid ! {string, Client_num, lists:reverse(S)}.
