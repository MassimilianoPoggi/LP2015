-module(lists_comp).
-export([squared_int/1, intersect/2, set_diff/2, list_diff/1]).

squared_int(L) -> [X*X || X <- L, is_integer(X)].
intersect(L1, L2) -> [X || X <- L1, lists:member(X, L2)].
% TOGLIENDO DOPPIE
set_diff(L1, L2) -> [X || X <- L1++L2, not(lists:member(X, intersect(L1, L2)))].
% SENZA TOGLIERE DOPPIE
list_diff(L1, L2) -> [X || X <- (L1--L2)++(L2--L1)].
