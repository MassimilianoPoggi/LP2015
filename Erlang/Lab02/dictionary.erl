-module(dictionary).
-export([start/0, lookup/1, store/2]).

init_node() ->
	erlang:set_cookie(node(), "Distributed Dictionary").

store(Key, Value) -> 
	store_aux(lookup(Key) == undefined, Key, Value).
store_aux(true, _, V) -> 
	{error, already_defined, value, V};
store_aux(false, Key, Value) -> 
	put(Key, Value).

lookup(Key) when get(Key) == undefined -> 
	rpc:multicall(nodes(), erland, get, [Key]),
	wait_lookup(Key, length(nodes());
lookup(Key) -> get(Key).

wait_lookup(0) -> undefined;
wait_lookup(N) -> 
	receive
		{_Pid, Message} when Message == undefined -> lookup_nodes(N-1);
		{_Pid, Value} -> Value
	end.
