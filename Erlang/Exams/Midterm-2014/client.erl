-module(client).
-export([is_prime/1, close/0]).

is_prime(N) ->
	send({new, N, self()}).

close() -> 
	send({quit, self()}).

send(Msg) ->
	{controller_pid, list_to_atom("serv@" ++ net_adm:localhost())} ! Msg,
	receive
		{result, R} -> io:format("~p~n", [R])
	end.
