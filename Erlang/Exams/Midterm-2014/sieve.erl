-module(sieve).
-export([init/1]).

init(N) ->
	receive
		{setup, Output, Next} -> loop(Output, Next, N)
	end.

loop(Output, Next, P) ->
	receive
		% can also do '{new, N} -> self() ! {pass, N};' instead of the three messages below.
		{new, N} when P * P > N -> 
			Output ! {res, true}, loop(Output, Next, P);
		{new, N} when N rem P == 0 -> 
			Output ! {res, false}, loop(Output, Next, P);
		{new, N} -> 
			Next ! {pass, N}, loop(Output, Next, P);

		{res, R} -> Output ! {res, R}, loop(Output, Next, P);

		{pass, N} when P * P > N -> 
			Output ! {res, true}, loop(Output, Next, P);
		{pass, N} when N rem P == 0 -> 
				Output ! {res, false}, loop(Output, Next, P);
		{pass, N} -> 
				Next ! {pass, N}, loop(Output, Next, P);

		Any -> io:format("Sieve ~p (~p) received message ~p.~n", [self(), P, Any]), loop(Output, Next, P)
	end.