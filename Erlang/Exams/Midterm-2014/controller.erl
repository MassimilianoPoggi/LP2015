-module(controller).
-export([start/1, primes_list/1]).

primes_list(N) -> [X || X <- lists:seq(2, N), length([Y || Y <- lists:seq(2, trunc(math:sqrt(X))), (X rem Y == 0)]) == 0].

start(Max) -> 
	register(controller_pid, self()),
	process_flag(trap_exit, true),
	FirstPid = create_ring(Primes = primes_list(Max)),
	loop(FirstPid, lists:last(Primes)).

create_ring(PList) ->
	link_ring([First|_Rest] = lists:map(fun (P) -> spawn_link(sieve, init, [P]) end, PList)),
	First. 

link_ring([First, Second|Tail]) ->
	First ! {setup, self(), Second},
	link_ring_sieves(First, [Second|Tail]).

link_ring_sieves(FirstPid, [Last]) ->
	Last ! {setup, FirstPid, FirstPid};
link_ring_sieves(FirstPid, [H1, H2|Tail]) ->
	H1 ! {setup, FirstPid, H2},
	link_ring_sieves(FirstPid, [H2|Tail]).

loop(FirstPid, LastPrime) ->
	receive
		{new, N, Client} when LastPrime * LastPrime > N -> 
			io:format("You asked for: ~p~n", [N]),
			FirstPid ! {new, N},
			receive
				{res, R} -> 
					Client ! {result, lists:flatten(io_lib:format("is ~p prime? ~p", [N, R]))}
			end,
			loop(FirstPid, LastPrime);

		{new, N, Client} ->
			io:format("You asked for: ~p~n", [N]),
			Client ! {result, lists:flatten(io_lib:format("~p is uncheckable, too big value.", [N]))},
			loop(FirstPid, LastPrime);

		{quit, Client} ->
			Client ! {result, "The service is closed!!!"},
			unregister(controller_pid),
			io:format("I'm closing ...~n");

		Any -> 
			io:format("Controller received message ~p.~n", [Any]),
			loop(FirstPid, LastPrime)
	end.