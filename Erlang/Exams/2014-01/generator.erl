-module(generator).
-export([start/3]).

% N = number of slaves (columns)
% M = max number in the combinations
% Pos = column of this generator (1 to N)
start(N, M, Pos) -> loop(1 > math:pow(M, N), N, M, Pos, 1).

loop(false, N, M, Pos, RowNum) ->
	%io:format("Generator at pos ~p currently doing iteration ~p.~n", [Pos, RowNum]),
	master_pid ! {result, (((RowNum - 1) div trunc(math:pow(M, N-Pos))) rem M) + 1, row, RowNum, col, Pos},
	loop(RowNum + 1 > math:pow(M, N), N, M, Pos, RowNum + 1);
loop(true, _N, _M, _Pos, _RowNum) -> 
	%io:format("Generator at pos ~p finished (RowNum = ~p, M^N = ~p).~n", [Pos, RowNum, trunc(math:pow(M, N))]),
	stop.