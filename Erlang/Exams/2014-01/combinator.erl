-module(combinator).
-export([start/2]).

start(N, M) ->
	register(master_pid, self()),
	lists:map(fun (Pos) -> spawn_link(generator, start, [N, M, Pos]) end, lists:seq(1, N)),
	loop(1, 1, trunc(math:pow(M, N)), N).

loop(CurrRow, _CurrCol, MaxRow, _MaxCol) when CurrRow > MaxRow -> unregister(master_pid);
loop(CurrRow, CurrCol, MaxRow, MaxCol) ->
	receive
		{result, R, row, CurrRow, col, CurrCol} when CurrCol == MaxCol-> 
			io:format("~p~n", [R]),
			loop(CurrRow + 1, 1, MaxRow, MaxCol);
		{result, R, row, CurrRow, col, CurrCol} -> 
			io:format("~p, ", [R]),
			loop(CurrRow, CurrCol + 1, MaxRow, MaxCol)
	end.