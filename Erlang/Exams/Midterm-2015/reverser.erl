-module(reverser).
-export([start/1]).

start(Index) ->
	receive
		{rev, S} -> merger_pid ! {res, lists:reverse(S), Index}
	end.