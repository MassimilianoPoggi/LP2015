-module(merger).
-export([start/0]).

start() ->
	register(merger_pid, spawn(fun () -> loop() end)).

loop() ->
	receive
		{rev, S, N, From} ->
			SlaveNum = start_slaves(N > length(S), S, N, 1),
			io:format("I got ~p and I'll ask ~p actors to reverse it!~n", [S, SlaveNum]),
			From ! {result, catch_replies(SlaveNum)},
			loop();

		{quit, From} ->
			io:format("I'm closing ...~n"),
			From ! {result, "The service has closed!!!"},
			unregister(merger_pid);

		Any ->
			io:format("Merger received unexpected message ~p.~n", [Any]),
			loop()
	end.

start_slaves(true, S, _N, I) ->
	spawn(reverser, start, [I]) ! {rev, S},
	I;
start_slaves(false, S, N, I) ->
	spawn(reverser, start, [I]) ! {rev, string:substr(S, 1, N)},
	% check should be N > length(string:substr(S, N+1)), this is equivalent
	% and does not call substr a second time
	start_slaves(2 * N > length(S), string:substr(S, N + 1), N, I + 1).

catch_replies(0) ->
	[];
catch_replies(SlaveNum) ->
	receive
		{res, S, SlaveNum} -> S ++ catch_replies(SlaveNum - 1)
	end.