-module(client).
-export([reverse_request/2, close/0]).

reverse_request(S, N) ->
	send({rev, S, N, self()}).

close() ->
	send({quit, self()}).

send(Msg) ->
	{merger_pid, 'serv@Massimiliano-PC'} ! Msg,
	receive
		{result, S} ->
			io:format("~p~n", [S]);

		Any ->
			io:format("Client received unexpected message ~p.~n", [Any])
	end.