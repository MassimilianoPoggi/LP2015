-module(ring).
-export([start/2, send_message/1, send_message/2, stop/0]).

start(N, Functions) -> 
	register(first_actor, start_aux(N, Functions)).

start_aux(1, [F]) -> 
	spawn(fun () -> loop_last(F) end);
start_aux(N, [F|Tail]) -> 
	Pid = start_aux(N - 1, Tail),
	spawn(fun () -> loop(F, Pid) end).
	
send_message(N) -> 
	send_message(N, 1).

send_message(N, M) ->
	send({call, N, M}).

stop() ->
	send({stop}).

send(Msg) ->
	first_actor ! Msg, ok.

loop(Function, Next) ->
	receive
		{call, N, M} -> Next ! {call, Function(N), M}, loop(Function, Next);
		{stop} -> Next ! {stop};
		Any -> io:format("Actor received message ~p.~n", [Any]), loop(Function, Next)
	end.

loop_last(Function) ->
	receive
		{call, N, 1} -> io:format("~p~n", [Function(N)]), loop_last(Function);
		{call, N, M} -> send_message(Function(N), M - 1), loop_last(Function);
		{stop} -> exit(normal);
		Any -> io:format("Last actor received message ~p.~n", [Any]), loop_last(Function)
	end.
