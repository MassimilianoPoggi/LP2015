-module(expressions).
-export([eval/1, parse/1]).

%% PARSING 
parse(S) -> {ok, Tokens, _} = expressions_lexer:string(S), {ok, Result} = expressions_parser:parse(Tokens), Result.

%% EVALUATION
eval({num, N}) -> N;
eval({plus, A, B}) -> eval(A) + eval(B);
eval({minus, A, B}) -> eval(A) - eval(B);
eval({minus, A}) -> -eval(A);
eval({mult, A, B}) -> eval(A) * eval(B);
eval({divide, A, B}) -> eval(A) div eval(B);
eval({condition, C, A, B}) -> eval_cond(eval(C) == 0, A, B).
eval_cond(true, A, _) -> eval(A);
eval_cond(false, _, B) -> eval(B).
