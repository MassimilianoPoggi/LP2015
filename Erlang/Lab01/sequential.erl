-module(sequential).
-export([is_palindrome/1, is_an_anagram/2, factor/1, is_proper/1]).

%% STRINGS
% FILTER
char_filter(S) -> [X || X <- string:to_lower(S), $a =< X, X =< $z].
% PALINDROME
is_palindrome(S) -> char_filter(S) == char_filter(lists:reverse(S)).
% ANAGRAM
is_an_anagram(S, [H|T]) -> lists:sort(char_filter(H)) == lists:sort(char_filter(S)) orelse is_an_anagram(S,T);
is_an_anagram(_, []) -> false.

%% NUMBERS
% LIST GENERATION
divisors_list(N) -> [X || X <- lists:seq(1, N), ((N rem X) == 0)].
prime_list(Max) -> [X || X <- lists:seq(2, Max), (length(divisors_list(X)) == 2)].
% FACTORIZATION
factor(N) -> factor_aux(N, prime_list(N)).
factor_aux(1, _) -> [];
factor_aux(N, [PH|PT]) when N rem PH == 0 -> [PH|factor_aux(N div PH, [PH|PT])];
factor_aux(N, [_|PT]) -> factor_aux(N, PT).
% PERFECT NUMBERS
is_proper(N) -> 2 * N == lists:foldl(fun (X, Sum) -> X + Sum end, 0, divisors_list(N)).