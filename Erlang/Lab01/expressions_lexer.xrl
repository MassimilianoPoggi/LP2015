Definitions.

Rules.

if : {token, {erlang:list_to_atom("if"), TokenLine}}.
then : {token, {erlang:list_to_atom("then"), TokenLine}}.
else : {token, {erlang:list_to_atom("else"), TokenLine}}.

[0-9]+ : {token, {num, TokenLine, erlang:list_to_integer(TokenChars)}}.
\( : {token, {'(', TokenLine}}.
\) : {token, {')', TokenLine}}.
- : {token, {'-', TokenLine}}.
\+ : {token, {'+', TokenLine}}.
\* : {token, {'*', TokenLine}}.
/ : {token, {'/', TokenLine}}.
[\s\n]+ : skip_token.

Erlang code.
