-module(ring).
-export([start_central/3, loop/2, start_distributed/3, create_next/2]).

%% COMMON
loop(Next_PID, Received) ->
	receive
		close_ring -> io:format("~p received stop message. Forwarding to ~p then closing.~n", [self(), Next_PID]), 
			Next_PID ! close_ring;
		Message -> io:format("~p received message ~p with text ~p. Forwarding to ~p.~n", 
			[self(), Received + 1, Message, Next_PID]), 
			Next_PID ! Message, loop(Next_PID, Received+1)
	end.

%% CENTRAL PROCESS
% CREATE WITH N - 1 IF YOU CONSIDER CENTRAL AS PART OF THE RING
start_central(M, N, Message) -> loop_central(M, Message, create_ring(N, self())).

create_ring(1, Central) -> Pid = spawn(ring, loop, [Central, 0]),
	io:format("Process ~p created process number ~p with PID ~p.~n", [self(), 1, Pid]), Pid;
create_ring(N, Central) -> Pid = spawn(ring, loop, [create_ring(N - 1, Central), 0]), 
	io:format("Process ~p created process number ~p with PID ~p.~n", [self(), N, Pid]), Pid.		

loop_central(0, _, First) -> io:format("Central sending close message.~n"),
	First ! close_ring;
loop_central(M, Message, First) ->
	io:format("Central sending message ~p to ~p (~p left to send).~n", [Message, First, M - 1]),
	First ! Message,
	receive
		Message -> io:format("Central received message ~p.~n", [Message]), 
			loop_central(M - 1, Message, First)
	end.

%% DISTRBUTED PROCESSES
start_distributed(M, N, Message) -> Pid = spawn(ring, create_next, [N - 1, self()]), 
	io:format("First process (~p) created process number ~p with PID ~p.~n", [self(), N - 1, Pid]), 
	loop_first(Pid, M, Message).

create_next(1, First) -> io:format("Process ~p is the last process.~n", [self()]), 
	loop(First, 0);
create_next(N, First) -> Pid = spawn(ring, create_next, [N - 1, First]), 
	io:format("Process ~p created process number ~p with PID ~p.~n", [self(), N - 1, Pid]), loop(Pid, 0).

loop_first(Next_PID, 0, _) ->
	io:format("First process sending close message.~n"),
	Next_PID ! close_ring;
loop_first(Next_PID, M, Message) ->
	io:format("First process sending message ~p to ~p (~p left to send).~n", [Message, Next_PID, M - 1]),
	Next_PID ! Message,
	receive
		Message -> 
			io:format("First process (~p) received message ~p.~n", [self(), Message]),
			loop_first(Next_PID, M - 1, Message)
	end.
