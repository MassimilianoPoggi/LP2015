Nonterminals E.
Terminals '(' ')' '+' '-' '*' '/' num if then else.

Rootsymbol E.

E -> '(' E ')' : '$2'.
E -> E '+' E : {plus, '$1', '$3'}.
E -> E '-' E : {minus, '$1', '$3'}.
E -> '-' '(' E ')' : {minus, '$3'}.
E -> E '*' E : {mult, '$1', '$3'}.
E -> E '/' E : {mult, '$1', '$3'}.
E -> if E then E else E : {condition, '$2', '$4', '$6'}.
E -> num : {num, extract_int('$1')}.

Erlang code.

extract_int({num, _Line, N}) -> N.