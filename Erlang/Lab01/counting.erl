-module(counting).
-export([start/0, call/1, stop/0]).

start() -> register(server_pid, spawn(fun () -> loop() end)).
call(Service) -> server_pid ! {call, Service}, ok.
stop() -> server_pid ! {stop}, ok.

get_number(Service) -> get_number_aux(get(Service)).
get_number_aux(undefined) -> 0;
get_number_aux(N) -> N.

print_table() -> lists:map(fun({Key, Value}) -> io:format("Service ~p has been called ~p times.~n", [Key, Value]) end, get()).

loop() ->
	receive
		{stop} -> io:format("Server is terminating.~n");
		{call, tot} -> put(tot, get_number(tot) + 1), print_table(), loop();
		{call, Service} -> put(Service, get_number(Service) + 1), put(tot, get_number(tot) + 1), loop()
	end.