-module(echo).
-export([start/0, print/1, stop_via_server/0, loop/0, start_client/0, stop_via_client/0, client/0]).

start() -> register(server_pid, spawn(echo, loop, [])), ok.

print(S) -> server_pid ! {msg, S}, ok.

stop_via_server() -> server_pid ! {stop}, ok.

loop() ->
	receive
		{msg, Message} -> io:format("Received message ~p.~n", [Message]), loop();
		{stop} -> io:format("Stopping execution.~n")
	end.

start_client() -> register(client_pid, spawn(echo, client, [])), ok.

stop_via_client() -> client_pid ! {stop}, ok.

client() ->
	link(whereis(server_pid)),
	receive
		{stop} -> 1 div 0 
	end.
