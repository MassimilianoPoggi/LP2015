open ComparableI;;
open IntervalI;;

module Interval(C : Comparable):(IntervalI with type endpoint = C.t) =
	struct
		type endpoint = C.t
		type interval = Empty | Interval of endpoint * endpoint
		
		exception WrongInterval 
		
		let create i_start i_end = if C.compare i_start i_end > 0 then raise WrongInterval 
											else Interval (i_start, i_end)

		let is_empty i = i = Empty

		let contains i x = match i with
			Empty -> false
			| Interval(i_start, i_end) -> C.compare i_start x <= 0 && C.compare x i_end <= 0

		let intersect i1 i2 = match i1,i2 with
			Empty, _ | _, Empty -> Empty 
			| Interval(i1_start, i1_end), Interval(i2_start, i2_end) ->
				let new_start = if (C.compare i1_start i2_start) < 0 then i2_start else i1_start and
					new_end = if (C.compare i1_end i2_end) < 0 then i1_end else i2_end
				in if (C.compare new_start new_end) <= 0 then Interval (new_start, new_end)
					else Empty

		let tostring i = match i with
			Empty -> "Empty"
			| Interval(i_start, i_end) -> "["^ (C.tostring i_start) ^ ", " ^ (C.tostring i_end) ^ "]"
	end;;

module IntInterval = Interval(struct
		type t = int
		let compare = compare 
		let tostring = string_of_int
	end);;

module StringInterval = Interval(struct
		type t = string
		let compare = String.compare 
		let tostring s = s
	end);;
