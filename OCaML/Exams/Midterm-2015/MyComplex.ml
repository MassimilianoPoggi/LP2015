(* COMPILE COMMANDS
ocamlc -c MyComplexI.mli
ocamlc -c MyComplex.ml
ocamlc -o main MyComplex.cmo main.ml
*)

module MyComplex : MyComplexI.MyComplexI =
	struct
		type complex = {real : float; im : float}

		let create ?(r=0.) i = {real = r; im = i}

		let (+/) a b = {real = a.real +. b.real; im = a.im +. b.im}

		let (-/) a b = {real = a.real -. b.real; im = a.im -. b.im}

		let ( */) a b = {real = a.real *. b.real -. a.im *. b.im;
							  im = a.im *. b.real +. a.real *. b.im} 

		let conj a = {real = a.real; im = -. a.im}

		let ($/) a n = {real = a.real /. n; im = a.im /. n}

		let (//) a b = (a */ (conj b)) $/ (b */ (conj b)).real

		let round_float f = 
			let msd = int_of_float (f *. 100.) and
			    rem = abs (int_of_float (f *. 1000.) mod 10) 
			in (if rem < 5 then (float_of_int msd) else (float_of_int (msd + (if f < 0. then -1 else +1)))) /. 100. 

		let pretty_print f = match f with
				_ when f = floor f -> string_of_int (int_of_float f)
				| _ -> string_of_float (round_float f)

		let tostring a = match a.real, a.im with
			r, 0. -> pretty_print r
			| 0., i -> (pretty_print i)^"i"
			| r, i -> (pretty_print r) ^"+"^(pretty_print i)^"i"
	end;;
