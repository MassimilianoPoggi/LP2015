module type DictSig =
	sig
		type key
		type value
		val null_value : value
		val is_null : value -> bool
	end;;

module StringIntDictSig = 
	struct
		type key = string
		type value = int
		let null_value  = 0
		let is_null n = n = 0
	end;;

module FloatCharDictSig =
	struct
		type key = float
		type value = char
		let null_value = char_of_int 0
		let is_null c = c = null_value
	end;;

module Dictionary (DS : DictSig) = 
	struct
		exception KeyNotFound
		let empty (_ : DS.key) = DS.null_value
		let add d k v = fun i -> if (k = i) then v else d i
		let find d k = if (DS.is_null (d k)) then raise KeyNotFound else d k
		let remove d k = if (DS.is_null (d k)) then raise KeyNotFound else (fun i -> if (i = k) then DS.null_value else d i)
	end;;

module SIDict = Dictionary(StringIntDictSig);;
module FCDict = Dictionary(FloatCharDictSig);;