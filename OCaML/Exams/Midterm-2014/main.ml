open Dictionary;;

let main() =
let si = SIDict.add (SIDict.add
(SIDict.add SIDict.empty "y" 25) "a" 1) "g" 7 in
print_string "si(y) :- ";
print_int (SIDict.find si "y") ;
print_endline "" ;
try
print_string "si(w) :- ";
print_int (SIDict.find si "w") ;
print_endline "" ;
with SIDict.KeyNotFound ->
print_endline "You are looking for the key \"w\"
that it is not in the dictionary!" ;
try
ignore(SIDict.remove si "e") ;
with SIDict.KeyNotFound ->
print_endline "The key \"e\" that you are trying to
remove is not in the dictionary!" ;
let si = SIDict.remove si "y" in
try
print_string "si(y) :- ";
print_int (SIDict.find si "y") ;
print_endline "" ;
with SIDict.KeyNotFound ->
print_endline "You are looking for the key \"y\"
that it is not in the dictionary!" ;
print_string "si(g) :- ";
print_int (SIDict.find si "g") ;
print_endline "" ;
let fc = FCDict.add (FCDict.add
(FCDict.add FCDict.empty 3.14 'p') 2.72 'e') 9.81 'g' in
print_string "fc(3.14) :- ";
print_char (FCDict.find fc 3.14) ;
print_endline "" ;
try
print_string "fc(3.1) :- ";
print_char (FCDict.find fc 3.1) ;
print_endline "" ;
with FCDict.KeyNotFound ->
print_endline "You are looking for the key 3.1
that it is not in the dictionary!" ;
try
ignore(FCDict.remove fc 3.1) ;
with FCDict.KeyNotFound ->
print_endline "The key 3.1 that you are trying to
remove is not in the dictionary!" ;
let fc = FCDict.remove fc 2.72 in
try
print_string "fc(2.72) :- ";
print_char (FCDict.find fc 2.72) ;
print_endline "" ;
with FCDict.KeyNotFound ->
print_endline "You are looking for the key 2.72
that it is not in the dictionary!" ;
print_string "fc(9.81) :- ";
print_char (FCDict.find fc 9.81) ;
print_endline "" ;;
main();;