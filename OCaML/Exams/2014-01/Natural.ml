module N:(NaturalI.NaturalI) =
	struct
		type natural = Zero | Succ of natural
		
		exception NegativeNumber
		exception DivisionByZero

		let rec ( + ) a b = match a,b with
			Zero, _ -> b
			| _, Zero -> a
			| _, Succ(n_b) -> Succ(a) + n_b

		let rec ( - ) a b = match a,b with
			_, Zero -> a
			| Zero, _ -> raise NegativeNumber
			| Succ(n_a), Succ(n_b) -> n_a - n_b

		let ( * ) a b = 
			let rec mult acc b_left = match b_left with
				Zero -> acc
				| Succ(n_b) -> mult (acc+a) n_b
			in mult Zero b

		let ( < ) a b = match a,b with
			_, Zero -> false
			| Zero, _ -> true
			| Succ(n_a), Succ(n_b) -> n_a < n_b

		let eval n =
			let rec eval acc n = match n with
				Zero -> acc
				| Succ(n_n) -> eval (succ acc) n_n
			in eval 0 n

		let convert n =
			let rec convert acc n = if n = 0 then acc else convert (Succ acc) (pred n)
			in convert Zero n

		let ( / ) a b =
			let rec div acc a_left = if a_left < b then acc else div (Succ acc) (a_left - b)
			in if b = Zero then raise DivisionByZero else div Zero a
	end;;