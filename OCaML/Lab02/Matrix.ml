module type MatrixSig =
	sig
		type t
		val add : t -> t -> t
		val mul : t -> t -> t
		val abs : t -> t
		val null: t
	end;;
	
module CharMatrixSig =
	struct 
		type t = char
		let add a b =  Char.chr ((Char.code a + Char.code b) mod 256) 
		let mul a b = Char.chr ((Char.code a * Char.code b) mod 256)
		let abs c = c
		let null = Char.chr 0
	end;;
	
module IntMatrixSig =
	struct
		type t = int
		let add a b = a + b 
		let mul a b = a * b
		let abs n = if n < 0 then -n else n
		let null = 0
	end;;

module Matrix (MS : MatrixSig) :
	sig
		type matrix = MS.t list list

		exception InvalidOperand
		exception InvalidOperandSize

		val equivalence : matrix -> matrix -> bool
		val copy : matrix -> matrix
		val add : matrix -> matrix -> matrix
		val scalar_mult : MS.t -> matrix -> matrix
		val transpose : matrix -> matrix
		val norm : matrix -> MS.t
		val matrix_mult : matrix -> matrix -> matrix
	end =
	struct
		type matrix = MS.t list list

		exception InvalidOperand
		exception InvalidOperandSize

		let is_valid m =
			let rec is_valid col_num = function
				[] -> true
				| h::tl when col_num = List.length h -> is_valid col_num tl
				| h::tl -> false
			in if (List.length m > 0) && (List.length (List.nth m 0) > 0) then is_valid (List.length (List.nth m 0)) (List.tl m) else false

		let equivalence m1 m2 =
			let rec equivalence v1 v2 = match v1 with
				[] -> true
				| h::tl when h = List.hd v2 -> equivalence tl (List.tl v2)
				| _ -> false
			in if not(is_valid m1) || not(is_valid m2) then raise InvalidOperand else 
				if (List.length m1 = List.length m2) && (List.length (List.nth m1 0) = List.length (List.nth m2 0)) then equivalence (List.flatten m1) (List.flatten m2) else false

		let copy m = m

		let add m1 m2 =
			let rec add m1 m2 = match m1 with
				[] -> []
				| h::tl -> List.map2 (MS.add) h (List.hd m2)::add tl (List.tl m2)
			in if not(is_valid m1) || not(is_valid m2) then raise InvalidOperand else 
				if (List.length m1 = List.length m2 && List.length (List.nth m1 0) = List.length (List.nth m2 0)) then add m1 m2 else raise InvalidOperandSize

		let scalar_mult n m = List.map (List.map (fun a -> MS.mul n a)) m

		let transpose m =
			let rec transpose m = match m with
				[]::tl -> []
				| h::tl -> (List.hd h::List.map List.hd tl)::transpose (List.map List.tl m)
				(* only to avoid warnings from the compiler/interpreter *)
				| [] -> []
			in if not(is_valid m) then raise InvalidOperand else transpose m

		let norm m =
			let rec norm m max = match m with
				[]::tl -> max
				| h::tl when (List.fold_left (fun a b -> MS.add (MS.abs b) a) (MS.null) (List.map List.hd m)) > max -> norm (List.map List.tl m) (List.fold_left (fun a b -> MS.add (MS.abs b) a) MS.null (List.map List.hd m))
				| h::tl -> norm (List.map List.tl m) max
				(* as above *)
				| [] -> max
			in if not(is_valid m) then raise InvalidOperand else norm m MS.null

		let matrix_mult m1 m2 =
			let rec matrix_mult m1 m2 = match m1 with
				[] -> []
				| h::tl -> List.map (function column -> List.fold_left (MS.add) (MS.null) (List.map2 (MS.mul) h column)) (transpose m2)::matrix_mult tl m2
			in if not(is_valid m1) || not(is_valid m2) then raise InvalidOperand else
				if (List.length (List.nth m1 0) = List.length m2) then matrix_mult m1 m2 else raise InvalidOperandSize
	end;;
	
module CharMatrix = Matrix(CharMatrixSig);;
module IntMatrix = Matrix(IntMatrixSIg);;