(* MONOIDS *)

module type OperatorSig =
	sig
		type t
		val set : t list
		val operator : t -> t -> t
		val operator_id_el : t
	end;;

module Monoid (OS : OperatorSig) :
	sig
		val test_associative : bool
		val test_identity : bool
		val test_closure : bool
	end =
	struct
		let test_associative_on a b c = ((OS.operator a (OS.operator b c)) = (OS.operator (OS.operator a b) c))

		let test_associative =
			let rec test_associative l1 l2 l3 = match l1 with
				[] -> true
				| h1::tl1 -> (match l2 with
									[] -> test_associative tl1 OS.set l3
									| h2::tl2 -> (match l3 with
														[] -> test_associative l1 tl2 OS.set
														| h3::tl3 when test_associative_on h1 h2 h3 -> test_associative l1 l2 tl3
														| h3::tl3 -> false))
			in test_associative OS.set OS.set OS.set
			
		let test_identity =
			let rec test_identity = function
				[] -> true
				| h::tl when OS.operator h OS.operator_id_el = h -> test_identity tl
				| h::tl -> false
			in test_identity OS.set

		let test_closure =
			let rec test_closure l = match l with
				[] -> true
				| h::tl when (List.fold_left (&&) true (List.map (fun member -> List.exists (fun a -> a = OS.operator h member) OS.set) OS.set)) -> test_closure tl
				| h::tl -> false
			in test_closure OS.set
	end;;

(* GROUPS *)

module Group (OS : OperatorSig) :
	sig
		val test_associative : bool
		val test_identity : bool
		val test_closure : bool
		val test_inverse : bool
	end = 
	struct
		include Monoid(OS)
		
		let test_inverse = 
			let rec test_inverse = function
				[] -> true
				| h::tl when List.fold_left (||) false (List.map (fun member -> OS.operator h member = OS.operator_id_el) OS.set) -> test_inverse tl
				| h::tl -> false
			in test_inverse OS.set
	end;;

(* RINGS *)

module type RingSig =
	sig
		type t
		val set : t list
		val add : t -> t -> t
		val mul : t -> t -> t
		val add_id_el : t
		val mul_id_el : t
	end;;

module Ring (RS : RingSig) :
	sig
		type check_op = GroupOp | MonoidOp

		val test_associative : check_op -> bool
		val test_identity : check_op -> bool
		val test_closure : check_op -> bool
		val test_inverse : bool
		val test_commutative : bool
		val test_distributive : bool
	end =
	struct
		type check_op = GroupOp | MonoidOp

		module GroupModule = Group(struct type t = RS.t let set = RS.set let operator = RS.add let operator_id_el = RS.add_id_el end)
		module MonoidModule = Monoid(struct type t = RS.t let set = RS.set let operator = RS.mul let operator_id_el = RS.mul_id_el end)

		let test_associative = function
			GroupOp -> GroupModule.test_associative
			| MonoidOp -> MonoidModule.test_associative

		let test_identity = function
			GroupOp -> GroupModule.test_identity
			| MonoidOp -> MonoidModule.test_identity

		let test_closure = function
			GroupOp -> GroupModule.test_closure
			| MonoidOp -> MonoidModule.test_closure

		let test_inverse = GroupModule.test_inverse

		let test_commutative =
			let rec test_commutative l1 l2 = match l1 with
				[] -> true
				| h1::tl1 -> (match l2 with
								[] -> test_commutative tl1 RS.set
								| h2::tl2 when RS.add h1 h2 = RS.add h2 h1 -> test_commutative l1 tl2
								| h2::tl2 -> false)
			in test_commutative RS.set RS.set

		let test_distributive =
			let rec test_distributive l1 l2 l3 = match l1 with
				[] -> true
				| h1::tl1 -> (match l2 with
								[] -> test_distributive tl1 RS.set l3
								| h2::tl2 -> (match l3 with
												[] -> test_distributive l1 tl2 RS.set
												| h3::tl3 when RS.mul h1 (RS.add h2 h3) = RS.add (RS.mul h1 h2) (RS.mul h1 h3) -> test_distributive l1 l2 tl3
												| h3::tl3 -> false))
			in test_distributive RS.set RS.set RS.set
	end;;