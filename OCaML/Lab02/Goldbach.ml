exception MathematicalDiscoveryException

let is_prime n =
	let rec is_prime i =
		if (i*i > n) then true
		else if n mod i = 0 then false
		else is_prime (i+2)
	in if n = 2 then true else if (n mod 2 = 0 || n <= 1) then false else is_prime 3;;

let goldbach n =
	let rec goldbach a =
		if (a > n-3) then raise MathematicalDiscoveryException
		else if is_prime(a) && is_prime(n-a) then (a,n-a)
		else goldbach (a+2)
	in if n = 4 then (2,2) else goldbach 3;;

let goldbach_range n m =
	let rec goldbach_range a =
		if (a > m) then []
		else (a, goldbach a)::goldbach_range (a+2)
	in if (n mod 2 = 1) then goldbach_range (n+1) else goldbach_range n;;