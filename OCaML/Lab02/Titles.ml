#load "str.cma"

let file_name = "prova.txt";;

(*READ*)

let get_word_list s = Str.split (Str.regexp "[ \t]+") s

let get_title_pairings sl line_number =
	let rec get_title_pairings word_index = function
		[] -> []
		| h::tl when String.length h > 2 && String.lowercase h <> "the" && String.lowercase h <> "and" -> 
			(h, line_number, word_index, List.fold_right (fun a b -> a^" "^b)  sl "")::get_title_pairings (word_index + String.length h + 1) tl
		| h::tl -> get_title_pairings (word_index + String.length h + 1) tl
	in get_title_pairings 0 sl;;

let get_file_lines =
	let rec read_lines ch line_num =
		try
			let line = input_line ch in
				(line_num, line)::read_lines ch (line_num + 1)
		with
			End_of_file -> (close_in ch; [])
	in read_lines (open_in file_name) 1

let read_file =
	let rec process_file_lines = function
		[] -> []
		| (line_num, title)::tl -> get_title_pairings (get_word_list title) line_num::process_file_lines tl
	in List.flatten (process_file_lines get_file_lines);;

(*PRINT*)

let sort title_list =
	let rec qsort = function
		[] -> []
		| (word, ln, wi, t)::tl -> qsort(List.filter (fun (w, _, _, _) -> w < word) tl) @ [(word, ln, wi, t)] @ 
			qsort(List.filter(fun (w, _, _, _) -> not(w < word)) tl)
	in qsort title_list;;

let get_formatted_title word_index title =
	let rec get_spaces_string n =
		if (n <= 0) then ""
		else " "^get_spaces_string (n-1)
	in let s = Str.string_after (get_spaces_string(40)^title) (word_index) in
		if (String.length s > 80) then Str.string_before s 80 else s;;

let print_title_list title_list =
	let rec print_title_list = function
		[] -> ()
		| (word, h_ln, h_wi, h_t)::tl -> ((Printf.printf("%5d %s\n") h_ln (get_formatted_title h_wi h_t)); print_title_list tl)
	in print_title_list (sort title_list);;

print_title_list(read_file);;
