let id x = x;;
let f_to_c x = (x -. 32.) *. 5. /. 9.;;
let c_to_f x = x *. 9. /. 5. +. 32.;;
let k_to_c x = x -. 273.15;;
let c_to_k x = x +. 273.15;;
let ra_to_c x = (x +. 273.15) *. 9. /. 5.;;
let c_to_ra x = (x -. 491.67) *. 5. /. 9.;;

type temp = C | K | F | Ra;;
let temp_list = [C; K ;F; Ra];;
let temp_f_list = [(C, id, id);(F, f_to_c, c_to_f);(K, k_to_c, c_to_k);(Ra, ra_to_c, c_to_ra)];;
exception ScaleNotFound;;

let convert_to_c (t, n) = 
	let rec search = function
		[] -> raise ScaleNotFound
		| (h, f, _)::tl when h = t -> f n
		| h::tl -> search tl
	in search temp_f_list;;

let convert_to_any (t, n) t2 =
	let rec search = function
		[] -> raise ScaleNotFound
		| (h, _, f)::tl when h = t2 -> f (convert_to_c (t,n))
		| h::tl -> search tl
	in search temp_f_list;;

let convert_to_all (t, n) =
	let rec search = function
		[] -> []
		| (h, _, f)::tl -> (h, f (convert_to_c (t, n)))::search tl
	in search temp_f_list;;