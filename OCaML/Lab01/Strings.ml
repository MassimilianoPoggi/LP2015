let is_letter c = 'a' <= Char.lowercase c && Char.lowercase c <= 'z';;

let rec is_palindrome s = if not (is_letter s.[0]) then is_palindrome (String.sub s 1 (String.length s - 1))
							else if not (is_letter s.[String.length s - 1]) then is_palindrome (String.sub s 0 (String.length s - 1))
							else if ((String.length s) < 2) then true 
							else (Char.lowercase s.[0] = Char.lowercase s.[String.length s - 1]) && (is_palindrome (String.sub s 1 ((String.length s) - 2)));;

let minus (*usare il - da problemi con i - usati per gli interi *) sa sb =
	let rec minus s i =
		if (i < String.length s) then
			if (String.contains sb s.[i]) then
				let res = (String.sub s 0 i)^(String.sub s (i+1) ((String.length s) - (i + 1))) in minus res i
			else
				minus s (i+1)
		else s
	in minus sa 0;; 

let rec count s c = match s with
	"" -> 0
	| _ -> if (s.[0] = c) then 1 + (count (String.sub s 1 ((String.length s) - 1)) c) else count (String.sub s 1 ((String.length s) - 1)) c;;

let rec is_anagram sa ?(i=0) sb = 
	if (i = String.length sa) then true
	else if (count sa sa.[i] = count sb sa.[i]) then is_anagram sa ~i:(i+1) sb else false;;

let rec contains_anagram s = function
	[] -> false
	| h::tl -> if (String.length h = String.length s && is_anagram (String.lowercase h) (String.lowercase s)) then true
				else contains_anagram s tl;;
