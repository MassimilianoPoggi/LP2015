let alk = [("beryllium", 4);("magnesium", 12);("calcium", 20);("strontium", 38);("barium", 56);("radium", 86)];;

let max l = 
	let rec max (emax, nmax) = function
		[] -> (emax, nmax)
		| (e, n)::tl -> if (n > nmax) then max (e, n) tl else max (emax, nmax) tl
	in max (List.hd l) (List.tl l);;

let qsort_number l =
	let rec qsort = function
		[] -> []
		| (e, n)::tl -> let (l1, l2) = List.partition (fun (a, b) -> b < n) tl in qsort l1 @ [(e, n)] @ qsort l2
	in qsort l;;

let qsort_name l =
	let rec qsort = function
		[] -> []
		| (e, n)::tl -> let (l1, l2) = List.partition (fun (a, b) -> String.compare a e < 0) tl in qsort l1 @ [(e, n)] @ qsort l2
	in qsort l;;

let gases = [("helium", 2);("neon", 10);("argon", 18);("krypton", 36);("xenon", 54);("radon", 86)];;