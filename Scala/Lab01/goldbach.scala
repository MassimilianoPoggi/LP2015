val divisors_list = (n: Int) => {
	for (i <- List.range(1, n + 1) if (n % i == 0)) yield i
}

val is_prime = (n: Int) => {
	divisors_list(n).length == 2
}

def goldbach (n: Int): (Int, Int) = { 
	(for (i <- List.range(2, n / 2 + 1) if (is_prime(i) && is_prime(n - i)))	yield (i, n-i)).head
}

val goldbach_range = (n: Int, m: Int) => {
	for (i <- List.range(n, m + 1, 2)) yield (i, goldbach(i))
}