import scala.collection.mutable.Stack

trait debug[Action, State] {
	def print (cmd: Action, result: State) = printf("Command %s executed.\nNew state: %s\n", cmd.toString, result.toString)
}

trait undo_redo[State] {
	private var undo_stack = new Stack[State]()
	private var redo_stack = new Stack[State]()

	def save(prev: State) {
		undo_stack.push(prev)
		redo_stack.clear
	}

	def undo(curr: State): Option[State] = {
		if (!undo_stack.isEmpty) {
			redo_stack.push(curr)
			val x = undo_stack.pop

			Some(x)
		} else None
	}

	def redo(curr: State): Option[State] = {
		if (!redo_stack.isEmpty) {
			undo_stack.push(curr)
			val x = redo_stack.pop

			Some(x)
		} else None
	}
}

class editor(var s: String) extends debug[String, (Int, String)] with undo_redo[(Int, String)] {
	var cursor = 0

	def x = {
		save((cursor, s))

		s = s.substring(0,cursor).concat(s.substring(cursor+1, s.length))
		
		print("x")
	}

	def dw = {
		save((cursor, s))

		val i = if (s.indexOf(' ', cursor) == -1) (s.length - 1) else s.indexOf(' ', cursor)
		s = s.substring(0,cursor).concat(s.substring(i, s.length))
		cursor = if (cursor == s.length) (cursor - 1) else cursor

		print("dw")
	}

	def i (arg: Character) = {
		save((cursor, s))

		cursor += 1
		s = s.substring(0,cursor).concat(arg.toString).concat(s.substring(cursor, s.length))

		print("i('" + arg + "')")
	}

	def iw (arg: String) = {
		save((cursor, s))

		//does the same thing but prints a million times since I added print at the end of each call to avoid typing it each time
		//for (c <- word.toCharArray) this.i(c)
		//this.i(' ')

		cursor += 1
		s = s.substring(0,cursor).concat(arg).concat(" ").concat(s.substring(cursor, s.length))
		cursor += arg.length + 1

		print("iw(" + arg + ")")
	}

	def print(cmd: String): Unit = print(cmd, (cursor, s))

	def l (arg: Int) = {
		save((cursor, s))

		cursor = math.min(cursor + arg, s.length -1)

		print("l(" + arg + ")")
	}

	def h (arg: Int) = {
		save((cursor, s))

		cursor = math.max(cursor - arg, 0)

		print("h(" + arg + ")")
	}

	def u = {
		undo((cursor, s)) match {
			case Some(x) => cursor = x._1; s = x._2
			case None => 
		}
		print("u")
	}

	def ctrlr = {
		redo((cursor, s)) match {
			case Some(x) => cursor = x._1; s = x._2
			case None => 
		}
		print("ctrlr")
	}
}