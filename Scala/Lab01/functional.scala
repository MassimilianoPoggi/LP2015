val is_palindrome = (S: String) => {
	val s = S.toLowerCase().filter { _.isLetter }
	s == s.reverse
}

val is_an_anagram = (S: String, SL: List[String]) => {
	(for (I <- SL if (S.toList.sorted.equals(I.toList.sorted))) yield true).length > 0
}

val divisors = (N: Int) => {
	for (I <- 1 until N + 1 if (N % I == 0)) yield I
}

val prime_list = (Max: Int) => {
	for (I <- 2 until Max + 1 if (divisors(I).length == 2)) yield I
}

val factors = (N: Int) => {
	for (I <- prime_list(N) if (N % I == 0)) yield I
}

val is_proper = (N: Int) => {
	divisors(N).fold(0) {(x,y) => x + y} == 2 * N
}
