object Titles {
	def get_words(title: String): List[String] = title.split(" ").toList.filter(!_.equals(""))

	//returns a List of tuples that contain (Line number, Indexed word, Word index, Full title)
	def get_title_pairings (title_line: (String, Int)): List[(Int, String, Int, List[String])] = {
		def parse_title(index: Int, words: List[String]): List[(Int, String, Int, List[String])] = {
			words match {
				case Nil => Nil
				case h::t if h.length > 2 && (!h.toLowerCase.equals("the")) && (!h.toLowerCase.equals("and")) => 
					(title_line._2 + 1, h.toLowerCase, index, get_words(title_line._1))::parse_title(index + h.length + 1, t)
				case h::t => parse_title(index + h.length + 1, t)
			}
		}

		parse_title(0, get_words(title_line._1))
	}

	def format_title(word_index: Int, title: String) = {
		def space_string(len: Int): String = {
			len match {
				case 0 => ""
				case n => " ".concat(space_string(n-1))
			}
		}

		val s = space_string(40).concat(title).substring(word_index)
		if (s.length > 80) s.substring(0, 80) else s
	}

	def main(args: Array[String]) = {
		val source = scala.io.Source.fromFile("prova.input")
		val lines = source.getLines.toList.zipWithIndex

		for (x <- (for (line <- lines) yield get_title_pairings(line)).flatten.sortBy(_._2)) {
			x match {
				case (line, _, windex, title) => printf("%5d %s\n", line, format_title(windex, title.mkString(" ")))
			}
		}

		source.close()
	}
}