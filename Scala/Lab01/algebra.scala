class AlgebraOperator[T](val set: List[T], val op: (T, T) => T, val op_id: T)
class RingOperator[T](val set: List[T], val add_op: (T, T) => T, val add_id: T, val mul_op: (T,T) => T, val mul_id: T)

class Monoid[T](val ao: AlgebraOperator[T]) {
	def test_associative: Boolean = {
		ao.set.map(el1 => ao.set.map(el2 => ao.set.map(el3 => ao.op(ao.op(el1, el2), el3) == ao.op(el1, ao.op(el2, el3)))).flatten).flatten.foldLeft(true) { (a,b) => a && b} 
	}

	def test_identity: Boolean = {
		ao.set.map(el => ao.op(el, ao.op_id) == el).foldLeft(true) { (a, b) => a && b }
	}

	def test_closure: Boolean = {
		ao.set.map(element1 => (ao.set.map(element2 => ao.set.contains(ao.op(element1, element2))))).flatten.foldLeft(true) { (a, b) => a && b }
	}
}

class Group[T](override val ao: AlgebraOperator[T]) extends Monoid[T](ao) {
	def test_inverse: Boolean = {
		ao.set.map(el1 => ao.set.map(el2 => ao.op(el1, el2) == ao.op_id).foldLeft(false) { (a, b) => a || b }).foldLeft(true) { (a, b) => a && b}
	}
}

class Ring[T](val ro: RingOperator[T]) {
	val m = new Monoid(new AlgebraOperator(ro.set, ro.add_op, ro.add_id))
	val g = new Group(new AlgebraOperator(ro.set, ro.mul_op, ro.mul_id))

	def test_associative = m.test_associative && g.test_associative

	def test_identity = m.test_identity && g.test_identity

	def test_closure = m.test_closure && g.test_closure

	def test_inverse = g.test_inverse

	def test_commutative = {
		ro.set.map(el1 => ro.set.map(el2 => ro.add_op(el1, el2) == ro.add_op(el2, el1))).flatten.foldLeft(true) { (a, b) => a && b}
	}

	def test_distributive = {
		ro.set.map(el1 => ro.set.map(el2 => ro.set.map(el3 => ro.mul_op(el1, ro.add_op(el2, el3)) == ro.add_op(ro.mul_op(el1, el2), ro.mul_op(el1, el3)))).flatten).flatten.foldLeft(true) { (a,b) => a && b}
	}
}