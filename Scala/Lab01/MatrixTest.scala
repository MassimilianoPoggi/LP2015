class Matrix(val m : List[List[Int]]) {
	val rows = m.length
	val cols = m.head.length

	def equivalent (that: Matrix): Boolean = {
		def equivalent_v (v1: List[Int], v2: List[Int]): Boolean = {
			(v1, v2) match {
				case (Nil, Nil) => true
				case (h1::t1, h2::t2) if h1 == h2 => equivalent_v(t1, t2)
				case _ => false
			}
		}

		equivalent_v (this.m.flatten, that.m.flatten)
	}

	def copy: Matrix = new Matrix(m)

	def addition (that: Matrix): Matrix = {
		def addition_m (m1: List[List[Int]], m2: List[List[Int]]): List[List[Int]] = {
			def addition_v (v1: List[Int], v2: List[Int]): List[Int] = {
				(v1, v2) match {
					case (Nil, Nil) => Nil
					case (h1::t1, h2::t2) => (h1 + h2)::addition_v(t1, t2)
					case _ => println("Error: invalid operand size"); Nil
				}
			}

			(m1, m2) match {
				case (Nil, Nil) => Nil
				case (h1::t1, h2::t2) => addition_v(h1, h2)::addition_m(t1, t2)
				case _ => println("Error: invalid operand size"); Nil
			}
		}

		new Matrix(addition_m(this.m, that.m))
	}

	def scalar_mult (n: Int): Matrix = new Matrix(m.map(_.map(el => el * n)))

	def transpose = {
		def tran(m: List[List[Int]]): List[List[Int]] = {
			m match {
				case Nil::tl => Nil
				case h::tl =>  m.map(_.head)::tran(m.map(_.tail))
				case Nil => Nil //not necessary, avoids warning
			}
		}

		new Matrix(tran(this.m))
	}

	def norm = {
		def norm_v(v: List[Int]): Int = {
			v match {
				case Nil => 0
				case h::tl => h.abs + norm_v(tl)
			}
		}

		m.map(norm_v(_)).max
	}

	def matrix_mult (that: Matrix): Matrix = {
		def mult_m(m1: List[List[Int]], tm2: List[List[Int]]): List[List[Int]] = {
			def mult_v(v1: List[Int], v2: List[Int]): Int = {
				(v1, v2) match {
					case (Nil, Nil) => 0
					case (h1::t1, h2::t2) => h1 * h2 + mult_v(t1, t2)
					case _ => println("Error: invalid operand size"); 0
				}
			}

			m1 match {
				case Nil => Nil
				case h::tl => tm2.map(mult_v(h, _))::mult_m(tl, tm2)
			}
		}

		new Matrix(mult_m(this.m, that.transpose.m))
	}

	override def toString = m.toString()
}

object MatrixTest {
	def main(args: Array[String]) = {
		val m = new Matrix(List(List(1,2,3),List(4,5,6)))
		val n = new Matrix(List(List(6,5,4),List(3,2,1)))

		println(m.addition(n))
		println(m.scalar_mult(5))
		println(m.transpose)
		println(m.norm)
		println(m.matrix_mult(m.transpose))
	}
}