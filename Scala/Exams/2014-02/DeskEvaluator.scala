import scala.util.parsing.combinator._
import scala.collection.mutable.HashMap

class DeskParser extends JavaTokenParsers {
	val assignments = new HashMap[Char, Int]

	def program = "print" ~> expr <~ "where" <~ decl ^^ { case n => println(n()); assignments }

	def expr = rep1sep((number_wrap | var_value), "+") ^^ { case list => list.foldLeft(() => 0) { case (f1: (() => Int), f2: (() => Int)) => (() => (f1() + f2())) } }

	def decl = rep1sep(assign, ",")
	def assign: Parser[Int] = var_name ~ ("=" ~> (assign | number)) ^^ { case name ~ value => assignments(name) = value; value }

	def number = wholeNumber ^^ { case n => n.toInt }
	def number_wrap = number ^^ { case n => (() => n)}
	def var_name = """[A-Za-z]""".r ^^ { n => n.charAt(0) }
	def var_value = var_name ^^ { v => () => assignments(v) }
}

object DeskEvaluator {
	def main(args: Array[String]) = {
		for (arg <- args) {
			val p = new DeskParser()
			val src = scala.io.Source.fromFile(arg)
			val lines = src.mkString

			p.parseAll(p.program, lines) match {
				case p.Success(result, _) => println(result)
				case p.Failure(msg, left) => println("Failure during parse: " + msg + ". Left: " + left)
				case p.Error(msg, left) => println("Error during parse: " + msg + ". Left: " + left)
			}
		}
	}
}