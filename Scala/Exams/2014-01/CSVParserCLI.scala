import scala.util.parsing.combinator._

class CSVParser extends JavaTokenParsers {
 	override val skipWhitespace = false
 	override val whiteSpace = """[ \t]""".r

	def file = rep(record)
	def record = rep1sep(field, ",") <~ """\r""".? <~ "\n"

	def field = whiteSpace.* ~> (without_commas | with_commas | "") <~ whiteSpace.*
	def without_commas = """[^\n,\"]+""".r
	def with_commas = "\"" ~> ("""[^\n\"]+""".r | "") <~ "\""
}

object CSVFormatter {
	def pretty_print(file: List[List[String]]) = {
		val col_sizes = file.map(_.map(_.length)).transpose.map(_.max)
		val row_len = col_sizes.reduceLeft { _ + _} + 3*col_sizes.length + 1

		def pretty_print_internal(row: List[String]) = {
			for ((field, field_size) <- row.zip(col_sizes)) {
				val format_string = "| %-" + field_size + "s "
				printf(format_string, field)
			}
			println("|")
		}

		println(Array.fill[Char](row_len)('-').mkString)
		pretty_print_internal(file.head)
		println(Array.fill[Char](row_len)('-').mkString)
		for (record <- file.tail) pretty_print_internal(record)
		println(Array.fill[Char](row_len)('-').mkString)
	}
}

object CSVParserCLI {
	def main(args: Array[String]) = {
		for (arg <- args) {
			val src = scala.io.Source.fromFile(arg)
			val lines = src.mkString
			val p = new CSVParser()

			p.parseAll(p.file, lines) match {
				case p.Success(result, _) => CSVFormatter.pretty_print(result)
				case p.Error(msg, _) => println("Error while parsing: " + msg)
				case p.Failure(msg, _) => println("Failure while parsing: " + msg)
			}
			println
			src.close
		}
	}
}