import scala.util.parsing.combinator._
import java.io._

class LongLangParser extends JavaTokenParsers {
	override protected val whiteSpace = """[ \t]+""".r
	def NEWLINE = """\r?\n""".r

	def program = repsep(task, NEWLINE.+)
	def task = "task" ~> task_name ~ (NEWLINE.* ~> "{" ~> NEWLINE.* ~> repsep(operation, NEWLINE.+) <~ NEWLINE.* <~ "}" <~ NEWLINE.* ) ^^
		{ case t ~ l =>
			println("Task " + t)
			var op_count = 0
			for (op <- l) {
				op_count += 1
				println("[op " + op_count + "] " + op)
			}
		}
	def operation = "remove" ~> file_name ^^ { s => LongLangExec.remove(s) } |
		"backup" ~> file_name ~ file_name ^^ { case s1 ~ s2 => LongLangExec.backup(s1, s2) } |
		"merge" ~> file_name ~ file_name ~ file_name ^^ { case s1 ~ s2 ~ s3 => LongLangExec.merge(s1, s2, s3) } |
		"rename" ~> file_name ~ file_name ^^ { case s1 ~ s2 => LongLangExec.rename(s1, s2) } 
	def file_name = stringLiteral ^^ { s => s.replaceAll("\"", "") }
	def task_name = """[A-Za-z0-9]+""".r
}

object LongLangExec {
	//DIRECT IO 
	def open(file: String) = {
		try {
			val src = scala.io.Source.fromFile(file)
			val lines = src.mkString
			src.close
			Some(lines)
		}
		catch {
			case e: Throwable => None
		}
	}

	def write(file: String, content: String) = {
		try {
			(new FileOutputStream(file, true)).write(content.getBytes())
			true
		}
		catch {
			case e: Throwable => false
		}
	}

	def remove(file: String) = {
		try {
			(new File(file)).delete
			true
		}
		catch {
			case e: Throwable => false
		}
	}
	//INDIRECT IO
	def merge(file_in_1: String, file_in_2: String, file_out: String) = {
		(open(file_in_1), open(file_in_2)) match {
			case (Some(f1), Some(f2)) => write(file_out, f1.concat("\n").concat(f2))
			case (_, None) | (None, _) => false
		}
	}

	def backup(file_in: String, file_out: String) = {
		(open(file_in)) match {
			case Some(f) => write(file_out, f)
			case None => false
		}
	}

	def rename(file_in: String, file_out: String) = {
		(open(file_in)) match {
			case Some(f) => 
				write(file_out, f) match {
					case true => remove(file_in); true
					case false => remove(file_out); false
				}	 
			case None => false
		}
	}
}

object LongLangEvaluator {
	def main(args: Array[String]) = {
		for (arg <- args) {
			val p = new LongLangParser()
			LongLangExec.open(arg) match {
				case Some(x) =>
					p.parseAll(p.program, x) match {
						case p.Success(_, _) =>
						case p.Error(msg, rest) => println("Parser error: " + msg + ". Left: " + rest.source.toString)
						case p.Failure(msg, rest) => println("Parser failure: " + msg + ". Left: " + rest.source.toString)
					}
				case None => println("File " + arg + " could not be opened.")
			}
		}
	}
}