object brainfuck_noparse {
	def run(program: Array[Char]) = {
		var program_counter = 0
		var memory = Array.fill[Int](30000)(0)
		var memory_pointer = 0

		def jump(dir: Int) = {
			var loop = 1
			while (loop > 0) {
				program_counter += dir
				loop += (program(program_counter) match {
					case '[' => dir
					case ']' => -dir
					case _ => 0
				})
			}
		}

		while (program_counter < program.length) {
			program(program_counter) match {
				case '>' => memory_pointer += 1
				case '<' => memory_pointer -= 1
				case '+' => memory(memory_pointer) += 1
				case '-' => memory(memory_pointer) -= 1
				case '.' => print(memory(memory_pointer).toChar)
				case ',' => memory(memory_pointer) = scala.io.StdIn.readChar
				case '[' => if (memory(memory_pointer) == 0) jump (+1)
				case ']' => if (memory(memory_pointer) != 0) jump (-1)
				case _ => 
			}
			program_counter += 1
		}
	}

	def main(args: Array[String]) = {
		val src = scala.io.Source.fromFile("prova2.input")
		val lines = src.mkString.toCharArray

		run(lines)
		src.close
	}
}