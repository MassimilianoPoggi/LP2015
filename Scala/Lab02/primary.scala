import scala.util.parsing.combinator._

/* GRAMMAR:
script = operation separator_line result_line

operation = line* ending_line
line = number "+" | number "-"
ending_line = number "="

separator_line = "-"*

result_line = number

number = ("+" | "-") decimalNumber
*/

class PrimaryParser extends JavaTokenParsers {
	override val whiteSpace = """[ \t]+""".r
	def acapo = sys.props("line.separator")

	def script = operation ~ (acapo ~> separator_line ~> acapo ~> number) ^^ { case a ~ b => a == b }

	def operation = repsep(line, acapo) ~ (acapo ~> ending_line) ^^ { case (h::tl) ~ n => tl.foldLeft(h) {case ((n1, op1), (n2, op2)) => (op1(n1, n2), op2)} match { case (value, op) => op(value, n)} }

	def line: Parser[(Int, (Int, Int) => Int)] = number ~ ("+" | "-") ^^ { case n ~ "+" => (n, (a:Int, b:Int) => a + b); case n ~ "-" => (n, (a:Int, b:Int) => a - b) }

	def ending_line = number <~ "="

	def separator_line = rep1("-")

	def number = (decimalNumber | "+" ~> decimalNumber) ^^ { _.toInt }| "-" ~> decimalNumber ^^ { -_.toInt }
}

object primary {
	def main(args: Array[String]) = {
		val src = scala.io.Source.fromFile("prova1.input")
		val lines = src.mkString

		val p = new PrimaryParser()
		p.parseAll(p.script, lines) match {
			case p.Success(result, _) => println(result);
			case p.Error(msg, _) => println("Error: " + msg);
			case p.Failure(msg, _) => println("Failure: " + msg)
		}

		src.close
	}
}