import scala.util.parsing.combinator._
import scala.collection.mutable.HashMap

class UnboundVariable(name: Char) extends RuntimeException(name.toString + " is not bound!")

/* GRAMMAR:
assign = variable_name "="" assign | expr
expr = term ("+ term | "- term)*
term = term ("*" fact | "/" fact)*
fact = number | variable_value |
*/
class ExpressionParserCombinator extends JavaTokenParsers {
	var assignments = new HashMap[Char, Float]()

	def expr_list: Parser[List[Float]] = rep1(assign)

	def assign: Parser[Float] = variable_name ~ ("=" ~> assign) ^^ { case name ~ value => assignments(name) = value; value } |
		expr

	def expr: Parser[Float]  = term ~ rep("+" ~ term | "-" ~ term) ^^ { case t ~ list => list.foldLeft(t) { case (acc,"+" ~ term) => t + term case (acc,"-" ~ term) => acc - term } }

	def term: Parser[Float] = fact ~ rep("*" ~ fact | "/" ~ fact) ^^ { case f ~ list => list.foldLeft(f) { case (acc,"*" ~ fact) => acc * fact case (acc,"/" ~ fact) => acc / fact } }

	def fact: Parser[Float] = ("(" ~> expr <~ ")" | number | variable_value | "+" ~> fact) |
		("-" ~> fact) ^^ { case a => -a }

	def number: Parser[Float] = (floatingPointNumber | decimalNumber) ^^ { case x => x.toFloat }

	def variable_value: Parser[Float] = variable_name ^^ { case x => if (!assignments.contains(x)) throw new UnboundVariable(x) else assignments(x) }

	def variable_name = """[A-Za-z]""".r ^^ {case x => x.charAt(0)}
}

object expressions {
	def main(args: Array[String]) = {
		val p = new ExpressionParserCombinator()
		val src = scala.io.Source.fromFile("prova.input");
		val lines = src.mkString

		try {
			p.parseAll(p.expr_list, lines) match {
				case p.Success(result, _) => println(result)
				case p.Error(msg, _) => println("Error: " + msg)
				case p.Failure(msg, _) => println("Failure: " + msg)
			}
		}
		catch { case e: Exception => print(e) }

		src.close
	}
}