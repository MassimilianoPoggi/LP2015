import scala.util.parsing.combinator._

abstract class Statement {
	def exec (pointer:Int, cells: Array[Int]): (Int, Array[Int])
}

case object IncreaseDataPointer extends Statement {
	override def exec (pointer: Int, cells: Array[Int]) = {
		(pointer+1, cells)
	}
}

case object EmptyStatement extends Statement {
	override def exec(pointer: Int, cells: Array[Int]) = {
		(pointer, cells)
	}
}

case object DecreaseDataPointer extends Statement {
	override def exec (pointer: Int, cells: Array[Int]) = {
		(pointer-1, cells)
	}
}

case object IncreaseDataCell extends Statement {
	override def exec (pointer: Int, cells: Array[Int]) = {
		cells(pointer) += 1
		(pointer, cells)
	}
}

case object DecreaseDataCell extends Statement {
	override def exec (pointer: Int, cells: Array[Int]) = {
		cells(pointer) -= 1
		(pointer, cells)
	}
}

case class Cycle(val statements: List[Statement]) extends Statement {
	override def exec (pointer: Int, cells: Array[Int]) = {
		var p = pointer
		var c = cells
		while (c(p) != 0) {
			for (i <- statements) {
				i.exec(p, c) match { case (a,b) => p = a; c = b }
			}
		}
		(p, c)
	}
}

case object WriteOutputByte extends Statement {
	override def exec (pointer: Int, cells: Array[Int]) = {
		print(cells(pointer).toChar)
		(pointer, cells)
	}
}

case object ReadInputByte extends Statement {
	override def exec (pointer: Int, cells: Array[Int]) = {
		print("Input a character: ")
		cells(pointer) = scala.io.StdIn.readChar
		(pointer, cells)
	}
}

class BrainfuckInterpreter extends JavaTokenParsers {
	def program = statement_list

	def statement_list: Parser[List[Statement]] = statement ~ statement_list.? ^^ { case s ~ Some(l) => s::l; case s ~ None => s::Nil}

	def statement: Parser[Statement] = 
		">" ^^ { case _ => IncreaseDataPointer } |
		"<" ^^ { case _ => DecreaseDataPointer } |
		"+" ^^ { case _ => IncreaseDataCell } |
		"-" ^^ { case _ => DecreaseDataCell } |
		"." ^^ { case _ => WriteOutputByte } |
		"." ^^ { case _ => ReadInputByte } | 
		"[" ~> statement_list <~ "]" ^^ { case list => new Cycle(list) } |
		spurious_str

	def spurious_str = """[^><+-.,\]\[]+""".r ^^ {case _ => EmptyStatement }

}

object brainfuck {
	def main(args: Array[String]) = {
		val src = scala.io.Source.fromFile("prova2.input")
		val lines = src.mkString

		val p = new BrainfuckInterpreter()
		p.parseAll(p.program, lines) match {
			case p.Success(list, _) => {
				var p = 0
				var c = new Array[Int](1000)
				for (i <- list) {
					i.exec(p, c) match { case (a,b) => p = a; c = b }
				}
			}
			case p.Error(msg, _) => println("Error: " + msg)
			case p.Failure(msg, _) => println("Failure: " + msg)
		}
	}
}